UNAME = $(shell uname -s)

CC := gcc
#CFLAGS := -Wall -Werror -g -D_BSD_SOURCE -std=c99
CFLAGS := -g -D_BSD_SOURCE -std=c99
LDFLAGS := -lpthread

SOURCES := client_main.c server_main.c ft_socket.c main.c

$(foreach,src,$(SOURCES),$(eval $(shell $(CC) $(CFLAGS) -MM $(src))))

ft: client_main.o server_main.o ft_socket.o main.o
	gcc -o ft client_main.o server_main.o ft_socket.o main.o $(LDFLAGS)
