#include <unistd.h>
#include <stdint.h>

#include "ft_socket.h"
#include "log.h"

int server_main(int argc, char **argv) {
	ft_socket_t sock = ft_socket();
	uint8_t buffer[256];
	int count;
	//int i = 0;

	FILE *fd = fopen("out", "w+");

	if (ft_bind(sock, 64000) != 0) {
		error(__FUNCTION__, "Failed to bind socket");
		return -1;
	}

	while (!ft_disconnected(sock)) {
		count = ft_recv(sock, (void *) buffer, 256);
		//verbose(__FUNCTION__, "Received %i bytes", count);
		//hexdump((char *) buffer, count);
		fwrite(buffer, sizeof(char), count, fd);
		//fwrite("\n\x40", sizeof(char), 2, fd);
		//extra("read data segment %i", i++);
		fflush(fd);
	}

	verbose(__FUNCTION__, "transmission ended");
	ft_close(sock);

	return 0;
}