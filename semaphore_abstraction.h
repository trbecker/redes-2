// Let's abstract semaphores to keep osx happy.
#ifndef __SEMAPHORE_ABSTRACTION__
#define __SEMAPHORE_ABSTRACTION__

#ifdef __APPLE__
# include <mach/mach.h>
# include <mach/task.h>
# include <mach/semaphore.h>

typedef semaphore_t sem_t;

#define sem_init(sem, policy, value) semaphore_create(mach_task_self(), sem, policy, value)
#define sem_destroy(sem) semaphore_destroy(mach_task_self(), sem)
#define sem_wait(sem) semaphore_wait(*sem)
#define sem_post(sem) semaphore_signal(*sem)
#define sem_close(sem)

#define SEM_SUCCESS KERN_SUCCESS

#else
// POSIX
# include <semaphore.h>
#define SEM_SUCCESS 0
#endif
#endif