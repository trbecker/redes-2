#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <pthread.h>

extern int _verbose;
extern int _debug;
extern int _hexdump;
extern FILE *logfile;
extern pthread_mutex_t log_mutex;

static inline void gbn_log(const char *level, const char *tag, const char *format, va_list args) {
	fprintf(stderr, "[%s/%s] ", tag, level);
	vfprintf(stderr, format, args);
	fprintf(stderr, "\n");
}

static inline void debug(const char *tag, const char *fmt, ...) {
	va_list args;

	if (!_debug)
		return;

	va_start(args, fmt);
	gbn_log("D", tag, fmt, args);
	va_end(args);
}

static inline void verbose(const char *tag, const char *fmt, ...) {
	va_list args;

	if (!_verbose)
		return;

	va_start(args, fmt);
	gbn_log("V", tag, fmt, args);
	va_end(args);
}

static inline void error(const char *tag, const char *fmt, ...) {
	va_list args;
	va_start(args, fmt);
	gbn_log("E", tag, fmt, args);
	va_end(args);
}

static inline void hex(const char *tag, const char *msg, const int len) {
#if 0
	if (!_verbose)
		return;

	fprintf(stderr, "[%s/V] ", tag);

	for (int i = 0; i < len; i++) {
		fprintf(stderr, "%x ", msg[i]);
	}

	fprintf(stderr, "\n");
#endif
}

static inline void msg(const char *tag, const char *msg, const int len) {
#if 0
	if (!_verbose)
		return;

	fprintf(stderr, "[%s/V] ", tag);

	for (int i = 0; i < len; i++) {
		fprintf(stderr, "%c", msg[i]);
	}

	fprintf(stderr, "\n");
#endif
}

static inline void rxl(const char *fmt, ...) {
	va_list args;

	pthread_mutex_lock(&log_mutex);

	fprintf(logfile, "[rx] ");
	va_start(args, fmt);
	vfprintf(logfile, fmt, args);
	va_end(args);

	fprintf(logfile, "\n");
	fflush(logfile);

	pthread_mutex_unlock(&log_mutex);
}

static inline void txl(const char *fmt, ...) {
	va_list args;

	pthread_mutex_lock(&log_mutex);

	fprintf(logfile, "[tx] ");
	va_start(args, fmt);
	vfprintf(logfile, fmt, args);
	va_end(args);

	fprintf(logfile, "\n");
	fflush(logfile);

	pthread_mutex_unlock(&log_mutex);
}

static inline void extra(const char *fmt, ...) {
	va_list args;

	pthread_mutex_lock(&log_mutex);

	fprintf(logfile, "[extra] ");
	va_start(args, fmt);
	vfprintf(logfile, fmt, args);
	va_end(args);

	fprintf(logfile, "\n");

	pthread_mutex_unlock(&log_mutex);
}

static inline void hexdump(const char *msg, const int len) {
	char b[35];

	if (!_hexdump)
		return;

	for (int k = 0; k < len; k += 8) {
		int i = 0, j = 26, l;
		for (l = k; l < k + 8 && l < len; l++) {
			char c = msg[l];
			char m = c < 32 || c == 127 ? '.' : c;
			sprintf(&b[i], "%02x ", c & 0xff);
			sprintf(&b[j], "%c", m);

			i += 3;
			j++;
		}

		for ( ; l < k + 8; l++) {
			b[i - 1] = b[i] = b[i + 1] = ' ';
			i += 3;
		}

		b[24] = ' ';
		b[25] = ' ';
		b[34] = '\0';
		fprintf(stderr, "%s\n", b);
	}
}
