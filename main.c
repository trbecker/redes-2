#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <time.h>

#include "log.h"

extern int server_main(int, char **);
extern int client_main(int, char **);

int _verbose = 1;
int _debug = 1;
int _hexdump = 1;
FILE *logfile;
pthread_mutex_t log_mutex;

int main(int argc, char **argv) {
	int client_mode = 0;

	logfile = stderr;

	pthread_mutex_init(&log_mutex, NULL);

	long seed = -1;
	

	for (int i = 1; i < argc; i++) {
		if (strcmp(argv[i], "-f") == 0) {
			client_mode = 1;
		} else if (strcmp(argv[i], "-s") == 0) {
			seed = atol(argv[++i]);
		} else if (strcmp(argv[i], "-l") == 0) {
			logfile = fopen("log.txt", "w+");
		}
	}

	if (seed == -1) {
		seed = (long) time(NULL);
	}

	verbose(__FUNCTION__, "seed = %ld", seed);
	srandom(seed);

	if (client_mode) {
		return client_main(argc, argv);
	} else {
		return server_main(argc, argv);
	}
}