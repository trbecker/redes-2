#ifndef FT_SOCKET
#define FT_SOCKET

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

typedef struct ft_socket *ft_socket_t;

ft_socket_t ft_socket();
ft_socket_t ft_client_socket();
void ft_close(ft_socket_t);

int ft_bind(ft_socket_t, int);

ssize_t ft_send(ft_socket_t, struct sockaddr *, void *, size_t);
ssize_t ft_recv(ft_socket_t, void *, size_t);

int ft_disconnected(ft_socket_t);

#endif