#include "ft_socket.h"

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <strings.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include <netdb.h>

#include <sys/select.h>

#include <pthread.h>
#include "semaphore_abstraction.h"
#include "log.h"

#include <assert.h>

#define WINDOW_SIZE					8
#define DATA_SIZE					250
#define FRAME_SIZE					(DATA_SIZE + 6)

#define SIGNAL_CLOSE    			0x00000001
#define SIGNAL_DISCONNECTED         0x00000002

#define RFRAME_SIZE					((FRAME_SIZE - 2) * 2 + 2)

#define FRAME_FLAG					(0x7e)
#define ESCAPE_FLAG					(0x7d)

#define GOOD_FCS					(0xf0b8)

#define seqn(frame)					((frame[2] >> 4) & 0x7)

#define FRAME_TYPE_MASK				0xc0
#define FRAME_TYPE_INFO				0x00
#define FRAME_TYPE_SUPR				0x80
#define FRAME_TYPE_LNCK				0xc0

#define SUPR_TYPE_MASK				0x30
#define SUPR_TYPE_RR				0x00
#define SUPR_TYPE_RNR				0x10

#define LNCK_DISC					(char) 0xc2
#define LNCK_UA						(char) 0xce

#ifndef FUCK_UP_CHANCE
#define FUCK_UP_CHANCE				10
#endif

struct window_entry {
	uint8_t frame[FRAME_SIZE];
	int size;
	long timestamp;
};

struct window {
	int first, last, release_ptr;
	sem_t read_permits, write_permits;
	pthread_mutex_t lock;
	struct window_entry entries[WINDOW_SIZE];
};

struct ft_socket {
	int sock;
	union {
		struct sockaddr_in in;
		struct sockaddr addr;
	} local;

	union {
		struct sockaddr_in in;
		struct sockaddr addr;
	} remote;

	pthread_t thread;

	uint32_t signals;

	struct window read_window;
	struct window write_window;

	int last_rr;
	long last_rnr_time;
};

int init_window(struct window *);
ssize_t receive(ft_socket_t, void *, size_t);
ssize_t unstuff(const void *, const size_t, void *, const size_t);
ssize_t stuff(const void *, const size_t, void *, const size_t);
static uint16_t pppfcs16(uint16_t, const unsigned char *, int);
void *socket_thread(void *);
int send_frame(ft_socket_t, char *, struct sockaddr *, size_t);
void post_rr(ft_socket_t, int);
void send_rr(ft_socket_t);
void send_rnr(ft_socket_t);
void process_rr(struct window *, int);
void process_rnr(struct window *, int);
int send_info_frame(ft_socket_t, struct window_entry *, struct sockaddr *, size_t);
int send_frame(ft_socket_t, char *, struct sockaddr *, size_t);
long gettimemillis();
void disconnect(ft_socket_t);

ft_socket_t ft_socket() {
	ft_socket_t sock;

	sock = (ft_socket_t) malloc(sizeof(struct ft_socket));

	if ((sock->sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		free(sock);
		return NULL;
	}

	if (init_window(&sock->read_window) < 0) {
		error(__FUNCTION__, "Failed to initilize read window");
		free(sock);
		return NULL;
	}

	if (init_window(&sock->write_window) < 0) {
		error(__FUNCTION__, "Failed to initilize write window");
		free(sock);
		return NULL;
	}

	if (pthread_create(&sock->thread, NULL, socket_thread, sock) != 0) {
		error(__FUNCTION__, "Failed to create thread");
		free(sock);
		return NULL;
	}

	sock->signals = SIGNAL_DISCONNECTED;
	sock->last_rr = -1;
	sock->last_rnr_time = 0;

	return sock;
}

ft_socket_t ft_client_socket() {
	ft_socket_t sock = ft_socket();

	int ww = (WINDOW_SIZE - 1) - (random() % WINDOW_SIZE);
	verbose(__FUNCTION__, "window size is %d", WINDOW_SIZE - ww - 1);

	for (int i = 0; i < ww; i++) {
		sem_wait(&sock->write_window.write_permits);
	}

	return sock;
}

void ft_close(ft_socket_t sock) {
	char buffer[FRAME_SIZE];
	int disconnect_ack = 0;

	if (!ft_disconnected(sock)) {
		sock->signals |= SIGNAL_CLOSE;
		pthread_join(sock->thread, NULL);

		disconnect(sock);

		while (!disconnect_ack) {
			int count = receive(sock, buffer, FRAME_SIZE);
			if (count >= 6 && buffer[2] == LNCK_UA) {
				rxl("U,UA");
				disconnect_ack = 1;
			}
		}
	} else {
		verbose(__FUNCTION__, "socket already disconnected");
		sock->signals |= SIGNAL_CLOSE;
		pthread_join(sock->thread, NULL);
	}
}

int ft_bind(ft_socket_t sock, int port) {
	bzero((char *) &sock->local.in, sizeof(struct sockaddr_in));
	sock->local.in.sin_family = AF_INET;
	sock->local.in.sin_addr.s_addr = htonl(INADDR_ANY);
	sock->local.in.sin_port = htons(port);

	return bind(sock->sock, &sock->local.addr, sizeof(struct sockaddr));
}

ssize_t ft_send(ft_socket_t sock, struct sockaddr *remote, void *buffer, size_t len) {
	int to_send = len;

	memcpy(&sock->remote.addr, remote, sizeof(struct sockaddr));

	while (to_send) {
		struct window *write_window = &sock->write_window;

		sem_wait(&write_window->write_permits);

		pthread_mutex_lock(&write_window->lock);
		int seq = write_window->last;
		write_window->last = (seq + 1) % WINDOW_SIZE;
		pthread_mutex_unlock(&write_window->lock);

		struct window_entry *entry = &write_window->entries[seq];

		size_t data_size = to_send >= DATA_SIZE ? DATA_SIZE : to_send;
	
		entry->frame[0] = FRAME_FLAG;
		entry->frame[1] = 0xff;
		entry->frame[2] = 0x00 | (seq & 0x7) << 4;

		memcpy(&entry->frame[3], buffer, data_size);

	
		entry->frame[data_size + 5] = FRAME_FLAG;

		entry->size = data_size + 6;

		send_info_frame(sock, entry, remote, data_size);

		sem_post(&write_window->read_permits);

		to_send -= data_size;
		buffer += data_size;
	}

	return len;
}

int send_frame(ft_socket_t sock, char *frame, struct sockaddr *remote, size_t data_size) {
	char rframe[RFRAME_SIZE];

	uint16_t *fcs = ((uint16_t *) &frame[data_size + 3]);
	*fcs = ~pppfcs16(0xffff, (unsigned char *) &frame[1], data_size + 2);

	ssize_t stuffed_size = stuff(frame, data_size + 6, rframe, RFRAME_SIZE);

	return sendto(sock->sock, rframe, stuffed_size, 0, remote, sizeof(struct sockaddr));
}

int send_info_frame(ft_socket_t sock, struct window_entry *entry, struct sockaddr *remote, size_t data_size) {
	char rframe[RFRAME_SIZE];

	uint16_t *fcs = ((uint16_t *) &entry->frame[data_size + 3]);
	*fcs = ~pppfcs16(0xffff, (unsigned char *) &entry->frame[1], data_size + 2);

	ssize_t stuffed_size = stuff(entry->frame, data_size + 6, rframe, RFRAME_SIZE);

	if ((random() % 100) < FUCK_UP_CHANCE) {
		int byte = random() % stuffed_size;
		rframe[byte] ^= 0x20;
		txl("I,%d <- corrompido", seqn(entry->frame));
	} else {
		txl("I,%d", seqn(entry->frame));
	}

	entry->timestamp = gettimemillis();
	return sendto(sock->sock, rframe, stuffed_size, 0, remote, sizeof(struct sockaddr));
}

ssize_t ft_recv(ft_socket_t sock, void *buffer, size_t len) {
	//verbose(__FUNCTION__, "waiting on semaphore");
	if (sem_wait(&sock->read_window.read_permits) != 0) {
		error(__FUNCTION__, "Failed to allocate semaphore %s", strerror(errno));
		return -1;
	}
	//verbose(__FUNCTION__, "read - 1");

	if (ft_disconnected(sock)) {
		return 0;
	}

	pthread_mutex_lock(&sock->read_window.lock);
	int current = sock->read_window.first;
	sock->read_window.first = (current + 1) % WINDOW_SIZE;
	//verbose(__FUNCTION__, "current is %i", current);

	struct window_entry *entry = &sock->read_window.entries[current];
	assert(entry->size > 0);
	int copy_size = len > (entry->size - 6) ? (entry->size - 6) : len;

	memcpy(buffer, &entry->frame[3], copy_size);
	pthread_mutex_unlock(&sock->read_window.lock);

	sem_post(&sock->read_window.write_permits);
	//verbose(__FUNCTION__, "write + 1");

	post_rr(sock, sock->read_window.first);

	return copy_size;
}

int ft_disconnected(ft_socket_t sock) {
	return sock->signals & SIGNAL_CLOSE;
}

int init_window(struct window *window) {
	if (sem_init(&window->write_permits, 0, WINDOW_SIZE - 1) != SEM_SUCCESS) {
		error(__FUNCTION__, "Failed to initialize semaphore");
		return -1;
	}

	if (sem_init(&window->read_permits, 0, 0) != SEM_SUCCESS) {
		error(__FUNCTION__, "Failed to initialize semaphore");
		return -1;
	}

	if (pthread_mutex_init(&window->lock, NULL) != 0) {
		error(__FUNCTION__, "failed to initialize mutex");
		return -1;
	}

	window->first = 0;
	window->last = 0;
	window->release_ptr = 0;

	return 0;
}

void resend_pending(ft_socket_t sock) {
	struct window *write_window = &sock->write_window;

	int n = write_window->last - write_window->first;
	if (n < 0) {
		n = WINDOW_SIZE + n;
	}

	int c = write_window->first;
	long current_time = gettimemillis();
	for (int i = 0; i < n; i++, c = (c + 1) % WINDOW_SIZE) {
		struct window_entry *entry = &write_window->entries[c];
		//extra("Checking frame %d (%li > %li)", c, current_time, entry->timestamp);
		if (current_time - entry->timestamp > 100) {
			send_info_frame(sock, entry, &sock->remote.addr, entry->size - 6);
		}
	}

	if (sock->last_rnr_time > 0 && gettimemillis() - sock->last_rnr_time > 2000) {
		send_rr(sock);
		sock->last_rnr_time = 0;
	}

}

void *socket_thread(void *data) {
	ft_socket_t sock = (ft_socket_t) data;
	char buffer[FRAME_SIZE];

	while (!(sock->signals & SIGNAL_CLOSE) || 
			sock->write_window.first != sock->write_window.last) {
		int count = receive(sock, buffer, FRAME_SIZE);
		//verbose(__FUNCTION__, "receive returned %d", count);

		if (ft_disconnected(sock)) {
			verbose(__FUNCTION__, "disconnected");
		}

		resend_pending(sock);

		if (count == 0) {
			continue;
		}

		if (buffer[2] == LNCK_DISC) {
			rxl("U,DISC");
			sock->signals = sock->signals | SIGNAL_CLOSE;

			char ack[6] = { FRAME_FLAG, 0xff, LNCK_UA, 0x0, 0x0, FRAME_FLAG };
			send_frame(sock, ack, &sock->remote.addr, 0);

			txl("U,UA");

			sem_post(&sock->read_window.read_permits);

			return NULL;
		}

		int frame_type = buffer[2] & FRAME_TYPE_MASK;

		if (frame_type == FRAME_TYPE_SUPR) {
			int supr_type = buffer[2] & SUPR_TYPE_MASK;
			int supr_seq = buffer[2] & 0x7;
			struct window *write_window = &sock->write_window;

			if (supr_type == SUPR_TYPE_RR) {
				process_rr(write_window, supr_seq);
			} else if (supr_type == SUPR_TYPE_RNR) {
				process_rnr(write_window, supr_seq);
			}

			continue;
		}

		struct window *read_window = &sock->read_window;

		sem_wait(&read_window->write_permits); // TODO change this to sem_trywait
		//verbose(__FUNCTION__, "write - 1");

		pthread_mutex_lock(&read_window->lock);
		int current = read_window->last;

		if (seqn(buffer) != current) {
			// rejected
			pthread_mutex_unlock(&read_window->lock);
			sem_post(&read_window->write_permits);
			//verbose(__FUNCTION__, "write + 1");
			rxl("I,%d <- OUT OF ORDER: expected %d", seqn(buffer), current);
			send_rr(sock);
			continue;
		}

		rxl("I,%d", current);

		read_window->last = (current + 1) % WINDOW_SIZE;

		struct window_entry *entry = &read_window->entries[current];

		memcpy(entry->frame, buffer, count);
		entry->size = count;

		pthread_mutex_unlock(&read_window->lock);

		sem_post(&read_window->read_permits);
		//verbose(__FUNCTION__, "read + 1");


		//int readv, writev;
		//sem_getvalue(&sock->read_window.read_permits, &readv);
		//sem_getvalue(&sock->read_window.write_permits, &writev);
		//verbose(__FUNCTION__, "waiting on select (%d, %d)", readv, writev);
		//assert(readv + writev == WINDOW_SIZE - 1);
	}

	return NULL;
}

void disconnect(ft_socket_t sock) {
	char frame[6] = { FRAME_FLAG, 0xff, LNCK_DISC, 0x0, 0x0, FRAME_FLAG };
	txl("UA,DISC");
	send_frame(sock, frame, &sock->remote.addr, 0);
}

void post_rr(ft_socket_t sock, int seq) {
	sock->last_rr = seq;
	if (random() % 100 < 10) {
		send_rnr(sock);
		sock->last_rnr_time = gettimemillis();
	} else {
		send_rr(sock);
	}
}

void send_rr(ft_socket_t sock) {
	char frame[6];

	if (sock->last_rr == -1 ||
		gettimemillis() - sock->last_rnr_time < 2000)
		return;

	frame[0] = FRAME_FLAG;
	frame[1] = 0xff;
	frame[2] = FRAME_TYPE_SUPR | SUPR_TYPE_RR | (sock->last_rr & 0x7);
	frame[5] = FRAME_FLAG;

	txl("RR,%d", sock->last_rr);
	send_frame(sock, frame, &sock->remote.addr, 0);
}

void send_rnr(ft_socket_t sock) {
	char frame[6];

	if (sock->last_rr == -1)
		return;

	frame[0] = FRAME_FLAG;
	frame[1] = 0xff;
	frame[2] = FRAME_TYPE_SUPR | SUPR_TYPE_RNR | (sock->last_rr & 0x7);
	frame[5] = FRAME_FLAG;

	txl("RNR,%d", sock->last_rr);
	send_frame(sock, frame, &sock->remote.addr, 0);	
}

static inline int subtract_modulo_window(int a, int b) {
	int r = a - b;
	if (r < 0)
		r = WINDOW_SIZE + r;
	return r;
}

void process_rr(struct window *window, int seq) {
	pthread_mutex_lock(&window->lock);
	// move the pointers to release_ptr = 0;
	int releases = subtract_modulo_window(seq, window->release_ptr);
	int pending = subtract_modulo_window(window->last, window->release_ptr);
	int first_adjusted = subtract_modulo_window(window->first, window->release_ptr);

	if (releases > pending) {
		rxl("RR,%d <- out of band", seq);
		// no releases will happen
		releases = 0;
	} else {
		rxl("RR,%d", seq);
		
		window->release_ptr = seq;
		if (first_adjusted < releases) {
			window->first = seq;
		}
	}

	pthread_mutex_unlock(&window->lock);

	for (int i = 0; i < releases; i++) {
		sem_post(&window->write_permits);
	} 

}

void process_rnr(struct window *window, int seq) {
	pthread_mutex_lock(&window->lock);
	int releases = subtract_modulo_window(seq, window->release_ptr);
	int pending = subtract_modulo_window(window->last, window->release_ptr);
	int first_adjusted = subtract_modulo_window(window->first, window->release_ptr);

	if (releases <= pending && releases > first_adjusted) {
		rxl("RNR,%d", seq);
		window->first = seq;
	} else {
		rxl("RNR,%d <- out of band", seq);
	}

	pthread_mutex_unlock(&window->lock);
}

ssize_t receive(ft_socket_t sock, void *buff, size_t buffer_len) {
	fd_set read_set;
	struct timeval timeout;
	int count;
	socklen_t socklen = sizeof(struct sockaddr);
	char *buffer = (char *) buff;

	char rframe[RFRAME_SIZE];

	timeout.tv_sec = 0;
	timeout.tv_usec = 100000;

	FD_ZERO(&read_set);
	FD_SET(sock->sock, &read_set);

	if ((count = select(sock->sock + 1, &read_set, NULL, NULL, &timeout)) == -1) {
		return errno;
	}

	if (count > 0 && FD_ISSET(sock->sock, &read_set)) {
		count = recvfrom(sock->sock, rframe, RFRAME_SIZE, 0, &sock->remote.addr, &socklen);
		count = unstuff(rframe, count, buffer, buffer_len);
		uint16_t fcs = pppfcs16(0xffff, (unsigned char *) &buffer[1], count - 2);
		if (fcs == GOOD_FCS) {
			return count;
		} else {
			rxl("BAD FCS, REJECTED");
			send_rr(sock);
		}
	}

	return 0;
}

static const uint16_t fcstab[256] = {
    0x0000, 0x1189, 0x2312, 0x329b, 0x4624, 0x57ad, 0x6536, 0x74bf,
    0x8c48, 0x9dc1, 0xaf5a, 0xbed3, 0xca6c, 0xdbe5, 0xe97e, 0xf8f7,
    0x1081, 0x0108, 0x3393, 0x221a, 0x56a5, 0x472c, 0x75b7, 0x643e,
    0x9cc9, 0x8d40, 0xbfdb, 0xae52, 0xdaed, 0xcb64, 0xf9ff, 0xe876,
    0x2102, 0x308b, 0x0210, 0x1399, 0x6726, 0x76af, 0x4434, 0x55bd,
    0xad4a, 0xbcc3, 0x8e58, 0x9fd1, 0xeb6e, 0xfae7, 0xc87c, 0xd9f5,
    0x3183, 0x200a, 0x1291, 0x0318, 0x77a7, 0x662e, 0x54b5, 0x453c,
    0xbdcb, 0xac42, 0x9ed9, 0x8f50, 0xfbef, 0xea66, 0xd8fd, 0xc974,
    0x4204, 0x538d, 0x6116, 0x709f, 0x0420, 0x15a9, 0x2732, 0x36bb,
    0xce4c, 0xdfc5, 0xed5e, 0xfcd7, 0x8868, 0x99e1, 0xab7a, 0xbaf3,
    0x5285, 0x430c, 0x7197, 0x601e, 0x14a1, 0x0528, 0x37b3, 0x263a,
    0xdecd, 0xcf44, 0xfddf, 0xec56, 0x98e9, 0x8960, 0xbbfb, 0xaa72,
    0x6306, 0x728f, 0x4014, 0x519d, 0x2522, 0x34ab, 0x0630, 0x17b9,
    0xef4e, 0xfec7, 0xcc5c, 0xddd5, 0xa96a, 0xb8e3, 0x8a78, 0x9bf1,
    0x7387, 0x620e, 0x5095, 0x411c, 0x35a3, 0x242a, 0x16b1, 0x0738,
    0xffcf, 0xee46, 0xdcdd, 0xcd54, 0xb9eb, 0xa862, 0x9af9, 0x8b70,
    0x8408, 0x9581, 0xa71a, 0xb693, 0xc22c, 0xd3a5, 0xe13e, 0xf0b7,
    0x0840, 0x19c9, 0x2b52, 0x3adb, 0x4e64, 0x5fed, 0x6d76, 0x7cff,
    0x9489, 0x8500, 0xb79b, 0xa612, 0xd2ad, 0xc324, 0xf1bf, 0xe036,
    0x18c1, 0x0948, 0x3bd3, 0x2a5a, 0x5ee5, 0x4f6c, 0x7df7, 0x6c7e,
    0xa50a, 0xb483, 0x8618, 0x9791, 0xe32e, 0xf2a7, 0xc03c, 0xd1b5,
    0x2942, 0x38cb, 0x0a50, 0x1bd9, 0x6f66, 0x7eef, 0x4c74, 0x5dfd,
    0xb58b, 0xa402, 0x9699, 0x8710, 0xf3af, 0xe226, 0xd0bd, 0xc134,
    0x39c3, 0x284a, 0x1ad1, 0x0b58, 0x7fe7, 0x6e6e, 0x5cf5, 0x4d7c,
    0xc60c, 0xd785, 0xe51e, 0xf497, 0x8028, 0x91a1, 0xa33a, 0xb2b3,
    0x4a44, 0x5bcd, 0x6956, 0x78df, 0x0c60, 0x1de9, 0x2f72, 0x3efb,
    0xd68d, 0xc704, 0xf59f, 0xe416, 0x90a9, 0x8120, 0xb3bb, 0xa232,
    0x5ac5, 0x4b4c, 0x79d7, 0x685e, 0x1ce1, 0x0d68, 0x3ff3, 0x2e7a,
    0xe70e, 0xf687, 0xc41c, 0xd595, 0xa12a, 0xb0a3, 0x8238, 0x93b1,
    0x6b46, 0x7acf, 0x4854, 0x59dd, 0x2d62, 0x3ceb, 0x0e70, 0x1ff9,
    0xf78f, 0xe606, 0xd49d, 0xc514, 0xb1ab, 0xa022, 0x92b9, 0x8330,
    0x7bc7, 0x6a4e, 0x58d5, 0x495c, 0x3de3, 0x2c6a, 0x1ef1, 0x0f78
};


static uint16_t pppfcs16(uint16_t fcs, const unsigned char *cp, int len) {
    assert(sizeof (uint16_t) == 2);
    assert(((uint16_t) -1) > 0);
    hex(__FUNCTION__, (char *) cp, len);
    while (len--)
        fcs = (fcs >> 8) ^ fcstab[(fcs ^ *cp++) & 0xff];

    return (fcs);
}

ssize_t stuff(const void *frame, const size_t flen, void *buffer, const size_t blen) {
	int i, j;
	assert (blen >= flen);

	((char *) buffer)[0] = ((char *) frame)[0];

	for (i = 1, j = 1; i < flen - 1 && j < blen - 1; i++, j++) {
		char d = ((char *) frame)[i];
		if (d == FRAME_FLAG || d == ESCAPE_FLAG) {
			((char *) buffer)[j] = ESCAPE_FLAG;
			d ^= 0x20;
			//verbose(__FUNCTION__, "buffer[%d] = %x", j, ((char *) buffer)[j] & 0xff);
			j++;
		}

		((char *) buffer)[j] = d;
		//verbose(__FUNCTION__, "buffer[%d] = %x", j, d & 0xff);
	}

	((char *) buffer)[j] = ((char *) frame)[flen - 1];

	return j + 1;
}

ssize_t unstuff(const void *rframe, const size_t rsize, void *buffer, const size_t size) {
	int i = 1, j = 1;

	((char *) buffer)[0] = ((char *) rframe)[0];

	for (i = 1, j = 1; j < size && i < rsize; i++, j++) {
		char b = ((char *) rframe)[i];
		if (b == 0x7d)
			b = ((char *) rframe)[++i] ^ 0x20;

		((char *) buffer)[j] = b;
	}

	return j;
}

long gettimemillis() {
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return tv.tv_sec * 1000 + tv.tv_usec / 1000;
}
