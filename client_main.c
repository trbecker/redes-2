#include <unistd.h>
#include <stdint.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include <netdb.h>

#include <strings.h>
#include <string.h>

#include <errno.h>

#include "ft_socket.h"
#include "log.h"

#define IO_SIZE 2500

int client_main(int argc, char **argv) {
	struct hostent *hostent;
	char *file, *host, buffer[IO_SIZE];
	FILE *fd;
	int i;

	host = "localhost";

	for (i = 1; i < argc; i++) {
		if (strcmp(argv[i], "-f") == 0) {
			file = argv[++i];
		} else if (strcmp(argv[i], "-h") == 0) {
			host = argv[++i];
		}
	}

	fd = fopen(file, "r");

	ft_socket_t sock = ft_client_socket();

	struct sockaddr_in remote;

	hostent = gethostbyname(host);
	if (hostent == NULL) {
		error(__FUNCTION__, "Unknown host %s: %s", host, strerror(errno));
		return 0;
	}

	verbose(__FUNCTION__, "Sending to %s", hostent->h_name);
	// TODO check for failure.

	bzero((char *) &remote, sizeof(struct sockaddr_in));

	remote.sin_family = AF_INET;
	remote.sin_port = htons(64000);
	bcopy((char *) hostent->h_addr_list[0], (char *) &remote.sin_addr, hostent->h_length);

	ft_bind(sock, 0);

	while (!feof(fd)) {
		int count = fread(buffer, sizeof(char), IO_SIZE, fd);
		//hexdump(buffer, count);
		ft_send(sock, (struct sockaddr *) &remote, buffer, count);
	}

	ft_close(sock);

	return 0;
}
